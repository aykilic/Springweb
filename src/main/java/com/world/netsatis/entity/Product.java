package com.world.netsatis.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ToString
@Table(name="Products")
public class Product extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO  )
    @Column(name="productId")
    private Long productid;
    @Column(length=50)
    private String productname;
//    private Long unitid;

    @OneToOne
    private Unit unit;

}
