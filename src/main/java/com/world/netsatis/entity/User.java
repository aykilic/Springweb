package com.world.netsatis.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="Users")
public class User extends BaseEntity {
    @Id
//    @SequenceGenerator(name="users_seq_gen",sequenceName = "users_gen",initialValue = 100,allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO  )
//    @Column(name="ID")
    private Long userid;
    @Column(length=50)
    private String firstname;
    @Column(length=50)
    private String lastname;
    @Column(length=50)
    private String email;
}
