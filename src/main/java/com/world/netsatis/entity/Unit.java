package com.world.netsatis.entity;

import com.world.netsatis.dto.ProductDto;
import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name="Units")
public class Unit extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO )
    @Column(name="unitId")
    private Long unitid;
    private String unitname;

//    @OneToOne()
//    private Product product;


}
