package com.world.netsatis.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ACCOUNT")
public class Account implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "accountId")
    private Integer accountId;

    @OneToOne(mappedBy = "account")
    private Employee employee;

    //Other fields, getters, setters are hidden for brevity
}