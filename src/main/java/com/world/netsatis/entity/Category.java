package com.world.netsatis.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="Categories")
public class Category extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO  )
    private Long categoryid;
    @Column(length=50)
    private String name;
    private Integer parentid;
}
