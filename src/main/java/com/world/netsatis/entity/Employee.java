package com.world.netsatis.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "EMPLOYEE")
public class Employee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employeeId")
    private Integer employeeId;

    @OneToOne
    @JoinColumn(name="ACCOUNT_ID")
    private Account account;

    //Other fields, getters, setters are hidden for brevity
}
