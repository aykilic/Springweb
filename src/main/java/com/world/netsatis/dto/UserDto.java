package com.world.netsatis.dto;

import lombok.Data;

@Data
public class UserDto {
    private Long userid;
    private String firstname;
    private String lastname;
}
