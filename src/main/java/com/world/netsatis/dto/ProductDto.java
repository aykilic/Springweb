package com.world.netsatis.dto;

import com.world.netsatis.entity.Unit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.OneToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private Long productid;
    private String productname;
    @OneToOne
    private UnitDto unit;

}
