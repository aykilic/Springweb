package com.world.netsatis.repository;

import com.world.netsatis.dto.ProductDto;
import com.world.netsatis.entity.Product;
import com.world.netsatis.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface ProductRepository extends JpaRepository<Product, Long> {

//    @Query("Select p,u From Product p left join Unit u on p.unitid=u.unitid" )
//    List<Product> getSorgu();
}