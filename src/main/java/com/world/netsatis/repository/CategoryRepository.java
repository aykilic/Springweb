package com.world.netsatis.repository;

import com.world.netsatis.entity.Category;
import com.world.netsatis.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CategoryRepository extends JpaRepository<Category, Long> {
}