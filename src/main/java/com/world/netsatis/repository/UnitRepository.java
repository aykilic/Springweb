package com.world.netsatis.repository;

import com.world.netsatis.entity.Unit;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UnitRepository extends JpaRepository<Unit, Long> {
}