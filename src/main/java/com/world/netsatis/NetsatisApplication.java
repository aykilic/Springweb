package com.world.netsatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NetsatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(NetsatisApplication.class, args);
    }

}
