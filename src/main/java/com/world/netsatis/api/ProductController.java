package com.world.netsatis.api;

import org.modelmapper.ModelMapper;
import com.world.netsatis.dto.ProductDto;
import com.world.netsatis.entity.Product;
import com.world.netsatis.repository.ProductRepository;
import com.world.netsatis.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/addproduct")
    public ResponseEntity<ProductDto> createProduct(@RequestBody ProductDto productDto){
        ProductDto product=productService.createProduct(productDto);
        return ResponseEntity.ok(product);
    }
    @GetMapping("/getproductall")
    public ResponseEntity<List<ProductDto>> getAllProduct(){
        List<ProductDto> productDto=productService.getAllProduct();
        return ResponseEntity.ok(productDto);

    }
    @GetMapping("/sorgu")
    public ResponseEntity<List<ProductDto>> getsorgu(){
          List<ProductDto> productDto=productService.getCustomAllProduct();
        return ResponseEntity.ok(productDto);
    }
}
