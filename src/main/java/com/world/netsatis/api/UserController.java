package com.world.netsatis.api;

import com.world.netsatis.dto.UserDto;
import com.world.netsatis.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/addUser")
    public ResponseEntity<UserDto> createUser(@RequestBody UserDto user) {
        UserDto resultUser = userService.createUser(user);
        return ResponseEntity.ok(resultUser);
    }

    @GetMapping("/getAllUsers")
    public ResponseEntity<List<UserDto>> GetAllUsers() {
        List<UserDto> resultUser = userService.getAllUsers();
        return ResponseEntity.ok(resultUser);
    }

    @GetMapping("/getById/{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable("id") Long Id) {
        UserDto resultUser = userService.getUser(Id);
        return ResponseEntity.ok(resultUser);


    }

    @PutMapping("/editUser/{id}")
    public ResponseEntity<UserDto> updateUser(@PathVariable("id") Long Id, @RequestBody UserDto user) {

        UserDto resultUser = userService.updateUser(Id, user);
        return ResponseEntity.ok(resultUser);

    }

    @DeleteMapping("/deleteUser/{id}")
    public ResponseEntity<Boolean> deleteUser(@PathVariable("id") Long Id) {
        Boolean status = userService.deleteUser(Id);
        return ResponseEntity.ok(status);
    }
}
