package com.world.netsatis.api;

import com.world.netsatis.dto.UnitDto;
import com.world.netsatis.dto.UserDto;
import com.world.netsatis.entity.Unit;
import com.world.netsatis.service.UnitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/unit")
public class UnitController{

    private final UnitService unitService;
    public UnitController(UnitService unitService) {
        this.unitService = unitService;
    }
    @PostMapping("/addunit")
    public ResponseEntity<UnitDto> createUnit(@RequestBody UnitDto unit){
        UnitDto resultUnit=unitService.createUnit(unit);
        return ResponseEntity.ok(resultUnit);
    }
    @GetMapping("/getallunits")
    public ResponseEntity<List<UnitDto>> getAllUnits(){
        List<UnitDto> resultUnits=unitService.getAllUnits();
        return ResponseEntity.ok(resultUnits);
    }
    @GetMapping("/getbyid/{id}")
    public ResponseEntity<UnitDto> getUser(@PathVariable("id") Long Id){
        UnitDto resultUnit=unitService.getUnit(Id);
        return ResponseEntity.ok(resultUnit);
    }
    @PutMapping("/updateunit/{id}")
    public ResponseEntity<UnitDto> updateUnit(@PathVariable("id") Long Id,@RequestBody UnitDto unit){
        UnitDto resultUnit=unitService.updateUnit(Id,unit);
        return ResponseEntity.ok(resultUnit);
    }
    @DeleteMapping("/deleteUnit/{id}")
    public ResponseEntity<Boolean> deleteUnit(@PathVariable("id")Long Id){
        Boolean resultUnit=unitService.deleteUnit(Id);
        return ResponseEntity.ok(resultUnit);
    }
}
