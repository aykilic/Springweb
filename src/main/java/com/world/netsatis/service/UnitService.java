package com.world.netsatis.service;

import com.world.netsatis.dto.UnitDto;

import java.util.List;

public interface UnitService {
    UnitDto createUnit(UnitDto unit) ;
    List<UnitDto> getAllUnits();
    UnitDto getUnit(Long id);
    UnitDto updateUnit(Long id,UnitDto unit);
    Boolean deleteUnit(Long id);
}
