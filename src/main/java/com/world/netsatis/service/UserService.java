package com.world.netsatis.service;

import com.world.netsatis.dto.UserDto;

import java.util.List;

public interface UserService {
    UserDto createUser(UserDto user) ;
    List<UserDto> getAllUsers();
    UserDto getUser(Long id);
    UserDto updateUser(Long id,UserDto user);
    Boolean deleteUser(Long id);
}
