package com.world.netsatis.service;

import com.world.netsatis.dto.ProductDto;
import com.world.netsatis.dto.UnitDto;
import com.world.netsatis.entity.Product;

import java.util.List;

public interface ProductService {
    ProductDto createProduct(ProductDto productDto) ;
    List<ProductDto> getAllProduct();
    List<ProductDto> getCustomAllProduct();

}
