package com.world.netsatis.service.Impl;

import com.world.netsatis.dto.ProductDto;
import com.world.netsatis.dto.UnitDto;
import com.world.netsatis.entity.Product;
import com.world.netsatis.entity.Unit;
import com.world.netsatis.repository.ProductRepository;
import com.world.netsatis.repository.UnitRepository;
import com.world.netsatis.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ModelMapper modelMapper;

    @Override
    public ProductDto createProduct(ProductDto productDto) {
        Product product=modelMapper.map(productDto,Product.class);
        product.setCreatedAt(new Date());
        product.setCreatedBy("Admin");
        product.setProductname(product.getProductname());
        product.setProductid(product.getProductid());
//      return productRepository.save(product);
        return modelMapper.map(productRepository.save(product),ProductDto.class);

    }

    @Override
    public List<ProductDto> getAllProduct() {
        List<Product> products=productRepository.findAll();
        List<ProductDto> dtos=products.stream().map(product->modelMapper.map(product,ProductDto.class)).collect(Collectors.toList());
        return dtos;

    }

    @Override
    public List<ProductDto> getCustomAllProduct() {
        List<Product> products=productRepository.findAll();
        List<ProductDto> dtos=products.stream().map(product->modelMapper.map(product,ProductDto.class)).collect(Collectors.toList());
        return dtos;
    }
}
