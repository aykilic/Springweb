package com.world.netsatis.service.Impl;

import com.world.netsatis.dto.UserDto;
import com.world.netsatis.entity.User;
import com.world.netsatis.repository.UserRepository;
import com.world.netsatis.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    @Override
    public UserDto createUser(UserDto userDto) {
        User user=modelMapper.map(userDto,User.class);
        user.setCreatedAt(new Date());
//        user.setCreatedBy("Admin");
        user.setCreatedBy(user.getFirstname());
        user.setCreatedBy(user.getLastname());

//        return userRepository.save(user);
        return modelMapper.map(userRepository.save(user),UserDto.class);
    }

    @Override
    public List<UserDto> getAllUsers() {
        List<User> users= userRepository.findAll();
        List<UserDto> dtos= users.stream().map(user->modelMapper.map(user,UserDto.class)).collect(Collectors.toList());
        return dtos;
    }

    @Override
    public UserDto getUser(Long id) {
        Optional<User> user=userRepository.findById(id);
        if (user.isPresent()){
            return modelMapper.map(user.get(),UserDto.class);
        }
        return null;
    }

    @Override
    public UserDto updateUser(Long id, UserDto user) {
        Optional<User> resultUser=userRepository.findById(id);
        if (resultUser.isPresent()){
            resultUser.get().setFirstname(user.getFirstname());
            resultUser.get().setLastname(user.getLastname());
            resultUser.get().setUpdatedBy("Admin");
            resultUser.get().setUpdatedAt(new Date());
            return modelMapper.map(userRepository.save(resultUser.get()),UserDto.class);
        }
        return null;
    }

    @Override
    public Boolean deleteUser(Long id) {
        Optional<User> resultUser=userRepository.findById(id);
        if (resultUser.isPresent()){
            userRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
