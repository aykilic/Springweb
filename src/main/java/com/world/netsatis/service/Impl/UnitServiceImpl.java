package com.world.netsatis.service.Impl;

import com.world.netsatis.dto.UnitDto;
import com.world.netsatis.dto.UserDto;
import com.world.netsatis.entity.Unit;
import com.world.netsatis.repository.UnitRepository;
import com.world.netsatis.service.UnitService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UnitServiceImpl implements UnitService {
    private final UnitRepository unitRepository;
    private final ModelMapper modelMapper;

    @Override
    public UnitDto createUnit(UnitDto unitDto) {
        Unit unit=modelMapper.map(unitDto,Unit.class);
        unit.setCreatedAt(new Date());
//     //   unit.setCreatedBy("Admin");
        unit.setUnitname(unit.getUnitname());

//        return userRepository.save(user);
        return modelMapper.map(unitRepository.save(unit),UnitDto.class);
    }

    @Override
    public List<UnitDto> getAllUnits() {
        List<Unit> units=unitRepository.findAll();
        List<UnitDto> dtos=units.stream().map(unit->modelMapper.map(unit,UnitDto.class)).collect(Collectors.toList());
        return dtos;
    }

    @Override
    public UnitDto getUnit(Long id) {
        Optional<Unit> unit=unitRepository.findById(id);
        if (unit.isPresent()){
            return modelMapper.map(unit.get(),UnitDto.class);
        }
        return null;
    }

    @Override
    public UnitDto updateUnit(Long id, UnitDto unit) {
        Optional<Unit> resultUnit=unitRepository.findById(id);
        if (resultUnit.isPresent()){
            resultUnit.get().setUnitname(unit.getUnitname());
            return modelMapper.map(unitRepository.save(resultUnit.get()), UnitDto.class);
        }
        return null;
    }

    @Override
    public Boolean deleteUnit(Long id) {
        Optional<Unit> resultUnit=unitRepository.findById(id);
        if (resultUnit.isPresent()){
            unitRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
